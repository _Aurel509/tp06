﻿using System;
using System.Collections.Generic;

namespace MesozoicConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur rené = new Dinosaur("René  MALLEVILLE", "BLKousaurus", 64);

            Horde horde = new Horde("Les relégables");
            Horde horde2 = new Horde("Olympique de Marseille");

            horde.addDino(louis); //Append dinosaur reference to end of list
            horde.addDino(nessie);
            horde2.addDino(rené);

            Console.WriteLine("\nIl y a dans la horde {0}, {1} dinosaures", horde.getName(), horde.getListDino());

            horde.sayHelloAll();
            Console.WriteLine("\nIl y a dans la horde {0}, {1} dinosaures", horde2.getName(), horde2.getListDino());
            horde2.sayHelloAll();

        }
    }
}
