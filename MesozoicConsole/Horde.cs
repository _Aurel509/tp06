using System;
using System.Collections.Generic;

namespace MesozoicConsole
{
    public class Horde
    {

        private string horde; 
        List<Dinosaur> list = new List<Dinosaur>();


        public Horde(string horde)
        {
            this.horde = horde;
        }

        public void addDino(Dinosaur dino){
            list.Add(dino);
        }

        public void removeDino(Dinosaur dino){
            list.Remove(dino);
        }

        public int getListDino(){
            return list.Count;
        }
        public List<Dinosaur> getHorde(){
            return list;
        }

        public string getName(){
            return this.horde;
        }

        public string sayHelloAll(){
            string test = "";
            foreach(Dinosaur dino in this.getHorde()){
                test = test + dino.sayHello();
            }
            return test;
        }




    }
}