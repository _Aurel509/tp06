using System;
using Xunit;

using MesozoicConsole;

namespace MesozoicTest
{
    public class HordeTest
    {
        [Fact]
        public void TestAddDino(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur trucmuche = new Dinosaur("Truc", "Cartonsaurus", 152);
            Horde groupe = new Horde("Raphasaurus");

            groupe.addDino(trucmuche);


            Assert.Equal(1, groupe.getListDino());

        }

        [Fact]
        public void TestSayHelloAll(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur trucmuche = new Dinosaur("Truc", "Cartonsaurus", 152);
            Horde groupe = new Horde("Raphasaurus");

            groupe.addDino(trucmuche);
            groupe.addDino(louis);

            Assert.Equal("Je suis Truc le Cartonsaurus, j'ai 152 ans.\nJe suis Louis le Stegausaurus, j'ai 12 ans.", groupe.sayHelloAll());
        }

    }
}
