using System;
using Xunit;

using MesozoicConsole;

namespace MesozoicTest
{
    public class DinosaurTest
    {
       /*  [Fact]
        public void TestDinosaurConstructor(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [Fact]
        public void TestGetName(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Louis", louis.getName());
        }
        [Fact]
        public void TestGetSpecie(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Stegausaurus", louis.getSpecie());
        }

        [Fact]
        public void TestGetAge(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal(12, louis.getAge());
        }

        /* [Fact]
        public void TestSetName(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal(, louis.setName("Michel"));
        }
        [Fact]
        public void testHug(){
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur michel = new Dinosaur("Michel", "Renésaurus", 11);
            Assert.Equal("Je suis Louis et je fais un calin à Michel", louis.hug(michel));
        }
        */
    }
}
